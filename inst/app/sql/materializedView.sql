-- Vue matérialisée pour l'application shiny

CREATE MATERIALIZED VIEW diagnostic_regional_etiages.vcn AS 
select cdstation,annee,mois,jour,date_deb,"VCN3" as valeur,'vcn3' as vcn
FROM hydrologie.vcn3
UNION
select cdstation,annee,mois,jour,date_deb,"VCN7" as valeur,'vcn7' as vcn
FROM hydrologie.vcn7
UNION
select cdstation,annee,mois,jour,date_deb,"VCN10" as valeur,'vcn10' as vcn
FROM hydrologie.vcn10
UNION
select cdstation,annee,mois,jour,date_deb,"VCN30" as valeur,'vcn30' as vcn
FROM hydrologie.vcn30
WITH DATA;
CREATE INDEX ON diagnostic_regional_etiages.vcn (cdstation);


-- Table indicateur_interannuel
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.indicateur_interannuel;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.indicateur_interannuel AS 
select cdstation,annee::numeric,NULL::numeric as mois,NULL::numeric as jour,"QMNA" as valeur,NULL as vcn, 'qmna' as indicateur
from hydrologie.qmna
UNION
SELECT cdstation,annee::numeric,NULL::numeric as mois,NULL::numeric as jour,"intensite_etiage_an" as valeur,NULL as vcn,'intensite_etiage' as indicateur
FROM hydrologie.intensite_etiages_annuel_qmna_qa
UNION
select cdstation,annee::numeric,mois::numeric,NULL::numeric as jour,"debit_moy_QMM" as valeur,NULL as vcn, 'qmm' as indicateur
from hydrologie.qmm
UNION
select cdstation,annee,mois,jour,"VCN3" as valeur,'vcn3' as vcn,'vcn' as indicateur
FROM hydrologie.vcn3
UNION
select cdstation,annee,mois,jour,"VCN7" as valeur,'vcn7' as vcn,'vcn' as indicateur
FROM hydrologie.vcn7
UNION
select cdstation,annee,mois,jour,"VCN10" as valeur,'vcn10' as vcn,'vcn' as indicateur
FROM hydrologie.vcn10
UNION
select cdstation,annee,mois,jour,"VCN30" as valeur,'vcn30' as vcn,'vcn' as indicateur
FROM hydrologie.vcn30
order by indicateur,cdstation,annee,mois,jour
WITH DATA;

CREATE INDEX ON diagnostic_regional_etiages.indicateur_interannuel (indicateur,cdstation,annee);

DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.indicateur_station;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.indicateur_station AS 
SELECT cdstation,"module_QA" as valeur,'qa' as indicateur
FROM diagnostic_regional_etiages.module
UNION
SELECT cdstation,"DCE" as valeur,'dce' as indicateur
FROM diagnostic_regional_etiages.debits_caracteristiques
UNION
SELECT cdstation,"etiage_abs" as valeur,'dea' as indicateur
FROM diagnostic_regional_etiages.debits_caracteristiques
UNION 
SELECT cdstation, "val_th" as valeur,'qmna5' as indicateur
FROM diagnostic_regional_etiages.qmna5_sec
order by indicateur,cdstation
WITH DATA;

CREATE INDEX ON diagnostic_regional_etiages.indicateur_station (indicateur,cdstation);

-- Episode assecs
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.episodes_assecs;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.episodes_assecs AS 
select *
from hydrologie.episodes_assecs
WITH DATA;
CREATE INDEX ON diagnostic_regional_etiages.episodes_assecs (indicateur,cdstation);



DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.liste_station_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.liste_station_epsg4326 AS 
SELECT distinct m.cdstation,
et.date_debut,et.annee_debut,et.mois_debut,et.date_fin,et.annee_fin,et.mois_fin,et.emprise_temp_annee,
et.nb_annee,et.emprise_temp_mois,et.classe,
sgeom.nom_station,sgeom.cdstation_nouveau, sgeom.type_station,sgeom.date_mservice,sgeom.date_fermeture,sgeom.cdsecteurh,sgeom.cdzonehyd,
sgeom.cdtronconh,sgeom.cdentitehy,sgeom.coordxstat,sgeom.coordystat,ST_Transform(sgeom.geom,4326) as geom ,sgeom.perc,
s.statut_station,s.finalite_station,s.influence,s.altitude,s.surface_bv_reel,s.surface_bv_topo,
s.qualite_basses_eaux,s.loi_basses_eaux,s.qualite_moy_eaux,s.qualite_hautes_eaux,s.loi_module,"RRSE",
me.cdbvspemdo, me.nombvspemd,
e.ecolo_etat, e.ecolo_conf, e.chim_etat, e.chim_ss_ubiq_etat, e.cdmassrapp,
ps.r_pponct,ps.r_pdiff,ps.r_hydro,ps.r_morpho, ps.r_micpol_eco, ps.r_micpol_eco_ssubi, ps.r_micpol_chim_ssubi
FROM diagnostic_regional_etiages.module as m
LEFT JOIN indicateur_station_hydro.emprise_temporelle_chronique_complete as et on et.cdstation=m.cdstation
LEFT JOIN reseau_suivi.stations_hydrographiques_epsg2154 as sgeom ON sgeom.cdstation=m.cdstation
LEFT JOIN reseau_suivi.fiche_station as s ON s.cdstation=m.cdstation
LEFT JOIN reseau_suivi.lien_stationh_bvme_nogeom as me ON me.cdstation=m.cdstation
LEFT JOIN edl_dce.etat_niveau1_rwbody_nogeom as e on e.cdmassrapp=me.cdbvspemdo
LEFT JOIN edl_dce.pression_significative_niveau1_rwbody_nogeom as ps on ps.eu_cd_rap=e.cdmassrapp
WHERE date_fermeture = ''
WITH DATA;

CREATE INDEX ON diagnostic_regional_etiages.liste_station (cdstation);

-- reseau_hydro_geom
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326 AS 
select oid,id,gid,cdentitehy,nomentiteh,candidat,classe, ST_Transform(geom,4326) as geom
from contexte_physique.reseau_hydro_2017_sandre_epsg2154
WITH DATA;

-- reseau_hydro_geom
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326 AS 
select oid,id,gid,cdentitehy,nomentiteh,candidat,classe, ST_Transform(geom,4326) as geom
from contexte_physique.reseau_hydro_2017_sandre_epsg2154
WITH DATA;

-- secteur_hydrographique_2017_sandre_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.secteur_hydrographique_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.secteur_hydrographique_2017_sandre_epsg4326 AS 
SELECT oid, id, gid, cdsecteurh, lbsecteurh, cdregionhy, lbregionhy, "Surface", libellsimple,ST_Transform(geom,4326) as geom
	FROM contexte_physique.secteur_hydrographique_2017_sandre_epsg2154
WITH DATA;

-- secteur_hydrographique_2017_sandre_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.secteur_hydrographique_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.secteur_hydrographique_2017_sandre_epsg4326 AS 
SELECT oid, id, gid, cdsecteurh, lbsecteurh, cdregionhy, lbregionhy, "Surface", libellsimple,ST_Transform(geom,4326) as geom
	FROM contexte_physique.secteur_hydrographique_2017_sandre_epsg2154
WITH DATA;

-- sous_secteur_hydrographique_2017_sandre_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.sous_secteur_hydrographique_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.sous_secteur_hydrographique_2017_sandre_epsg4326 AS 
SELECT oid, id, gid, cdsoussect, lbsoussect, cdsecteurh, lbsecteurh, cdregionhy, lbregionhy, ST_Transform(geom,4326) as geom
FROM contexte_physique.sous_secteur_hydrographique_2017_sandre_epsg2154
WITH DATA;

-- zones_hydrographiques_sandre2017_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.zones_hydrographiques_sandre2017_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.zones_hydrographiques_sandre2017_epsg4326 AS 
SELECT oid, id, gid, cdzonehydr, lbzonehydr, cdsoussect, lbsoussect, cdsecteurh, lbsecteurh, cdregionhy, lbregionhy, idnoeudhyd, cdentitehy, pkexutoire, ST_Transform(geom,4326) as geom
FROM contexte_physique.zones_hydrographiques_sandre2017_epsg2154
WITH DATA;

-- zones_hydrographiques_sandre2017_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.zones_hydrographiques_sandre2017_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.zones_hydrographiques_sandre2017_epsg4326 AS 
SELECT oid, id, gid, cdzonehydr, lbzonehydr, cdsoussect, lbsoussect, cdsecteurh, lbsecteurh, cdregionhy, lbregionhy, idnoeudhyd, cdentitehy, pkexutoire, ST_Transform(geom,4326) as geom
FROM contexte_physique.zones_hydrographiques_sandre2017_epsg2154
WITH DATA;

-- masses_eau_cours_eau_edl2019_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.masses_eau_cours_eau_edl2019_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.masses_eau_cours_eau_edl2019_epsg4326 AS 
SELECT oid, id, cdmassedea, cdeumassed, nommassede, systemeref, cdeubassin, typemassed, cdeussbass, cdcategori, cdnaturema, catgeol, longtotkm, strahlmax, strahlmin, taillefcts, etendusurf, ST_Transform(geom,4326) as geom
FROM edl_dce.masses_eau_cours_eau_edl2019_epsg2154;
WITH DATA;

-- bv_masse_eau_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.bv_masse_eau_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.bv_masse_eau_epsg4326 AS 
SELECT oid, id, cdbvspemdo, nombvspemd, niveauprec, surfacebvs, cdcategori, cdmassedea, commentbvs, ST_Transform(geom,4326) as geom
	FROM edl_dce.bv_masse_eau_epsg2154;
WITH DATA;
