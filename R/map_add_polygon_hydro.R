#' Ajout du reseau hydrographique
#' @param .map un objet leaflet
#' @param reseau_hydro un objet sf en epsg 4326
#' @param groupe nom du groupe
#' @return une carte leaflet avec les fonds de plan et les differents boutons
#' @export
#' @import dplyr 
#' @import leaflet 
#'
map_add_polygon_hydro <- function(.map,reseau_hydro,groupe){

  map <- .map %>%
            leaflet::addPolylines(
            data=reseau_hydro,
            weight = 1,
            opacity = 0.9,
            color = "#303030",
            fillOpacity = 0.9,
            group=groupe
            )

  return(map)
}