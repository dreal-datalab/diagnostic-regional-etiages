#' @title get_reseau_hydro_geom
#' @description Extraction de la liste des indicateurs
#' @param con Parametre de connexion d'un DBI::dbConnect
#' @param type_hydro Extraction au choix ("secteur","sous_secteur","zones","meso")
#' @importFrom glue glue
#' @importFrom sf st_read st_zm
#' @return objet sf
#' @export
#' 

get_reseau_hydro_geom <- function(con,type_hydro){
  
  if(type_hydro=="bdcarthage"){
    table_geom <- "reseau_hydro_2017_sandre_epsg4326"
  }else if(type_hydro=="meso"){
    table_geom <- "masses_eau_cours_eau_edl2019_epsg4326"
  }else{}

  query <- glue::glue("SELECT *
                       FROM diagnostic_regional_etiages.{table_geom}")

  data_geom <- sf::st_read(con, query = query) %>%
                 sf::st_zm()


  return(data_geom)
}
