#' @title chart_intensite_etiage
#' @description Construction d'un graphique pour representer l'intensite des etiages
#' @param data_chart Donnees utiles pour representer l'intensite des etiages
#' @return plot echarts4r
#' @import echarts4r
#' @import dplyr
#' @import lubridate
#' @export
#' @source https://github.com/JohnCoene/echarts4r/issues/99

chart_intensite_etiage <- function(data_chart){

    data_chart %>%
        echarts4r::e_charts(annee) %>%
        echarts4r::e_title(text = "Intensit\u00e9 de l\'\u00e9tiage") %>%
        echarts4r::e_legend(left = 500) %>%
        echarts4r::e_bar(serie = valeur) %>%
        echarts4r::e_y_axis(name = "D\u00e9bit (m3/s)", nameLocation = "middle", nameGap = 50) %>%
        echarts4r::e_toolbox() %>%
        echarts4r::e_toolbox_feature(feature = c("saveAsImage"), title = "T\u00e9l\u00e9charger") %>%
        echarts4r::e_tooltip(
          trigger = "item",
          axisPointer = list(
            type = "cross"
          )
        )
}

                 
