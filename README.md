# dataviz-influence-etiage

<!-- badges: start -->
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

Application de visualisation de données des résultats du diagnostic régional des étiages en région Pays-de-la-Loire : 

L'application est développée en R-Shiny avec le package [golem](https://github.com/ThinkR-open/golem).

![](inst/extdata/architecture_app.png)

## Base de données

L'application s'appuie sur une base de données PostgreSQL/PostGIS (Version 13). 

## Dépendances

- Liste des packages utilisés :

```r
> attachment::att_from_rscripts()
 [1] "config"          "dplyr"           "shinyjs"         "base64enc"      
 [5] "waiter"          "echarts4r"       "lubridate"       "glue"           
 [9] "data.table"      "DBI"             "sf"              "leaflet"        
[13] "leaflegend"      "shinycssloaders" "DT"              "shinyWidgets"   
[17] "wesanderson"     "ggplot2"         "classInt"        "htmltools"      
[21] "tibble"          "htmlwidgets"     "stringr"
```

- Installation des packages nécessaires :

```r
install.packages("attachment")
attachment::install_from_description()
```

## Application R-Shiny

Le dockerfile du dépôt complète le descriptif.

### Déploiement


#### En local, dans l'environnement de développement

1. Configurer la connexion (objet `con`) à la base de données dans le fichier `R/run_dev` :

```r
con <- DBI::dbConnect(
    RPostgres::Postgres(),
    dbname = dbname,
    host = host,
    port = port,
    user = user,
    password = password)

```

2. A la racine du projet, lancer la commande suivante :

```r
golem::run_dev()
```

### En local, en installant l'application

A la racine du projet :

1. Installer le paquet en local : 

``` r
remotes::install_local(upgrade="never")
```

2. Lancer à la racine du projet les commandes suivantes :

``` r
con <- DBI::dbConnect(
    RPostgres::Postgres(),
    dbname = dbname,
    host = host,
    port = port,
    user = user,
    password = password)

datavizinfluenceetiage::run_app(con)
```

### En local, dans une image docker

- Configuration :

```bash
# Construction du docker
sudo docker build . -t datavizinfluenceetiage --build-arg CACHEBUST=$(date +%s)

# Lancer le docker run avec les variables d'environnement de connexion à la BDD
sudo docker run \
-e DB_NAME='DB_NAME' \
-e DB_HOST='IP' \
-e DB_PORT='PORT' \
-e DB_USER='DB_USER' \
-e DB_PW='PW' \
--name datavizinfluenceetiage 
```

### Serveur shinyProxy de développement

Le déploiement s'effectue en continue, voir le fichier `.gitlab.ci.yml`

- Accès : 

A définir

### Shiny proxy externe

- Configuration pour le 1er déploiement

```bash
# Construction du docker dans 
cd ~/
git clone git@gitlab.com:dreal-datalab/diagnostic-regional-etiages.git
cd ~/dataviz-influence-etiage

sudo docker build . -t datavizinfluenceetiage --build-arg CACHEBUST=$(date +%s)

# Ajouter l'application dans l'application.yml
sudo vim ~/shinyproxy_kit/shinyproxy/application.yml

- id: datavizinfluenceetiage
    display-name: "Dataviz sur l'influence de l'étiage"
    description: "Dataviz sur l'influence de l'étiage"
    container-cmd: ["R", "-e", "options('shiny.port'=3838,shiny.host='0.0.0.0');con <- DBI::dbConnect(RPostgres::Postgres(),dbname = Sys.getenv('DB_NAME'),host = Sys.getenv('DB_HOST'),port =Sys.getenv('DB_PORT'),user = Sys.getenv('DB_USER'),password = Sys.getenv('DB_PW'));datavizinfluenceetiage::run_app(con=con)"]
    container-image: datavizinfluenceetiage
    container-network: sp-example-net
    container-volumes: ["/var/log/shinyproxy:/var/log/shinyproxy"]
    container-env:
        DB_NAME: *******
        DB_HOST: *******
        DB_PORT: *******
        DB_USER: *******
        DB_PW: *******

# Ajout d'une vignette
cp ~/shinyproxy_kit/apps/dataviz-influence-etiage/inst/extdata/datavizinfluenceetiage.png ~/shinyproxy_kit/shinyproxy/templates/2col/assets/img

# Redémarrer shinyproxy
cd ~/shinyproxy_kit/shinyproxy/
docker-compose stop
docker-compose build shinyproxy
docker-compose up -d
```

- Mise à jour de l'application

```bash
cd ~/shinyproxy_kit/shinyproxy/apps/
git reset --hard HEAD
git pull origin main

docker build . -t datavizpfas --build-arg CACHEBUST=$(date +%s)
```
